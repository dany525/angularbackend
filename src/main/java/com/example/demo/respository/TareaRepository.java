package com.example.demo.respository;

import org.springframework.stereotype.Repository;
import com.example.demo.entity.Tarea;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface TareaRepository extends CrudRepository<Tarea, Long> {
	// @Query("select t from Tarea t where t.tarea_id=:idTarea }")
	// public Tarea getTareaForId(@Param("idTarea") Long idTarea);
	
	public void guardar(Tarea entity);
	public Iterable<Tarea> findAll();

}
