package com.example.demo.respository;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import com.example.demo.entity.Tarea;

public class TareaRepositoryImpl implements TareaRepository {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional
	public void guardar(Tarea entity) {
		em.persist(entity);
	}


	@Override
	public Iterable<Tarea> findAll() {
		StringBuilder query = new StringBuilder();
		query.append("select t from Tarea t");

		return em.createQuery(query.toString()).getResultList();
	}


	@Override
	public void deleteById(Long id) {
		Tarea tarea = new Tarea();
		tarea.setTarea_id(id.intValue());
		em.remove(em.merge(tarea));
		
	}

	@Override
	public void delete(Tarea entity) {
		em.remove(entity);
		
	}


	@Override
	public <S extends Tarea> S save(S entity) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <S extends Tarea> Iterable<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Optional<Tarea> findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public Iterable<Tarea> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void deleteAll(Iterable<? extends Tarea> entities) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	
}
