
package com.example.demo.restController;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Tarea;
import com.example.demo.respository.TareaRepository;

@RequestMapping("/Tarea")
@RestController
public class TareaRestController {
	@Autowired
	private TareaRepository tareaRepository;

	@RequestMapping
	public ResponseEntity<Iterable<Tarea>> tareas() {

		return new ResponseEntity<Iterable<Tarea>>(tareaRepository.findAll(), HttpStatus.OK);

	}
	/*
	 * @RequestMapping("/tarea") public ResponseEntity<Tarea>
	 * getTareaById(@RequestParam Long idTarea){
	 * 
	 * return new ResponseEntity<Tarea>(tareaRepository.getTareaForId(idTarea),
	 * HttpStatus.OK); }
	 */

	@RequestMapping("/crearTarea")
	public boolean crearTarea(@RequestParam(value = "descripcion") String descripcion,
			@RequestParam(value = "fecha_ejecucion") String fecha_ejecucion,
			@RequestParam(value = "user") int user) {
		System.out.print("descripcion " + descripcion + "fecha_ejecucion" + fecha_ejecucion + "usuario" + user);
		Tarea tarea = new Tarea();
		tarea.setDescripcion(descripcion);
		//15-02-2019
		//String startDate = "06/27/2007";
		
		DateFormat df = new SimpleDateFormat("dd-mm-yyyy");
		try {
			Date fechaEjec = df.parse(fecha_ejecucion);
			tarea.setFecha_ejecucion(fechaEjec);
			tarea.setFecha_creacion(new Date());
			tarea.setEstado(1);
			tarea.setUsuario_id(user);
			tareaRepository.guardar(tarea);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	@RequestMapping("/borrarTarea")
	public boolean borrarTarea(@RequestParam(value = "idTarea") Long idTarea) {
	   tareaRepository.deleteById(idTarea);
		return false;
	}

}
