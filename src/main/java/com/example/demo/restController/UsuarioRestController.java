package com.example.demo.restController;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.ResourceAccessException;

import com.example.demo.entity.Usuario;
import com.example.demo.respository.UsuarioRepository;
import com.example.demo.service.UserService;




@RequestMapping("/Usuarios")
@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT,RequestMethod.DELETE})
public class UsuarioRestController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping() 
	public List<Usuario> getUsuarios(){
		return userService.getUsuarios();
	}
	
	 @PostMapping("/addUser")
	    public Usuario createEmployee(@Valid @RequestBody Usuario usuario) {
	        return userService.createEmployee(usuario);
	  }
	 
	 
	 @PutMapping("/{id}")
	 public ResponseEntity<Usuario> updateEmployee(@PathVariable(value = "id") Long usuarioId,
	  @Valid @RequestBody Usuario userdetail)throws Exception{  
		return userService.updateEmployee(usuarioId, userdetail);
	 }
		

	 @DeleteMapping("/{id}")
	 public boolean borrarUsuario(@PathVariable(value = "id") long idUsuario) {
		return userService.borrarUsuario(idUsuario);
	 }

}
