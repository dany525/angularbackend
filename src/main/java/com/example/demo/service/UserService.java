package com.example.demo.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.entity.Usuario;
import com.example.demo.respository.UsuarioRepository;              

@Service 
public class UserService {
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	public List<Usuario> getUsuarios(){
		List<Usuario>  getAll = usuarioRepository.findAll();
		return getAll;
	}
	
	
	  public Usuario createEmployee(@Valid @RequestBody Usuario usuario) {
	        return usuarioRepository.save(usuario);
	  }
	  
	  
	  public ResponseEntity<Usuario> updateEmployee(@PathVariable(value = "id") Long usuarioId,
		     @Valid @RequestBody Usuario userdetail)throws Exception{  
	        Usuario user = new Usuario();
            user.setId(usuarioId);
            user.setIdNumber(userdetail.getIdNumber());
            user.setCorreo(userdetail.getCorreo());
            user.setEstado(userdetail.getEstado());
            user.setFechaIngreso(userdetail.getFechaIngreso());
            user.setFechaRegistro(userdetail.getFechaRegistro());
            user.setPrimerApellido(userdetail.getPrimerApellido());
            user.setSegundoApellido(userdetail.getSegundoApellido());
            user.setPrimerNombre(userdetail.getPrimerNombre());
            user.setSegundoNombre(userdetail.getSegundoNombre());
            user.setIdArea(userdetail.getIdArea());
            user.setIdpais(userdetail.getIdpais());
            user.setTipoId(userdetail.getTipoId());
            final Usuario updatedEmployee = usuarioRepository.save(user);
		    return ResponseEntity.ok(updatedEmployee);
	  }
		
	  
	  public boolean borrarUsuario(@PathVariable(value = "id") long idUsuario) {
			usuarioRepository.deleteById(idUsuario);
			return true;
	  }
	 
	
}
