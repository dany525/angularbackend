package com.example.demo.entity;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tarea")
public class Tarea {
	@Id
	@Column(name="tarea_id")
	private long tarea_id;
	@Column(name="descripcion")
	private String descripcion;
	@Column(name="fecha_ejecucion")
	private Date fecha_ejecucion;
	@Column(name="fecha_creacion")
	private Date fecha_creacion;	
	@Column(name="estado")
	private int estado;
	@Column(name="usuario_id")
	private int usuario_id;
	
	public Tarea() {
		super();
	}

	public long getTarea_id() {
		return tarea_id;
	}

	public void setTarea_id(int tarea_id) {
		this.tarea_id = tarea_id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha_ejecucion() {
		return fecha_ejecucion;
	}

	public void setFecha_ejecucion(Date fecha_ejecucion) {
		this.fecha_ejecucion = fecha_ejecucion;
	}

	public Date getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(Date fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getUsuario_id() {
		return usuario_id;
	}

	public void setUsuario_id(int usuario_id) {
		this.usuario_id = usuario_id;
	}
	
	
	

}
